package praktikum5;

public class Rist {

	public static void main(String[] args) {

		int tabeliSuurus = 10;
		String ridatext = "";
		
		for (int rida = 0; rida <= tabeliSuurus; rida++ ) {
			for (int tulp = 0; tulp <= tabeliSuurus; tulp++ ) {
				if (rida == tulp || rida + tulp == tabeliSuurus)
					ridatext += "1";
				else 
					ridatext += "0";
			}
			ridatext += "\r";
		}
		System.out.println(ridatext);
	}
}
