package praktikum5;

public class Tsyklid {

	public static void main(String[] args) {

		if (true) {
			System.out.println("Tingimus vastab tõele");
		}
		
		int j = 0;
		while (j < 3) {
			System.out.println("Tingimus vastab tõele (while)");
			j++; // i = i + 1;
		}
		
		for (int i = 0; i < 10; i++) {
			System.out.println("for tsükkel, i: " + i);
		}

		String ridatext = "";
		for (int tulp = 0; tulp <= 10; tulp += 2 ) {
			ridatext += tulp; 
		}
		System.out.println(ridatext);
		
		System.out.println();
		
		
		ridatext ="";
		
		for (int rida = 0; rida <= 10; rida++ ) {
			for (int tulp = 0; tulp <= 10; tulp++ ) {
				ridatext += "0"; 
			}
			ridatext += "\r";
		}
		System.out.println(ridatext);

	}	
}