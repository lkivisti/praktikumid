package praktikum2;

import praktikum1.TextuuIO;

public class Inimesed {

	public static void main(String[] args) {
		System.out.println("Inimeste arv: ");
		int people = TextuuIO.getInt();
		System.out.println("Grupi suurus: ");
		int groupSize = TextuuIO.getInt();
		System.out.println(String.format("Gruppe %1$d, ülejäänud %2$d", people / groupSize, people % groupSize));
	}
}
