package praktikum2;

import praktikum1.TextuuIO;
import java.util.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

public class MituAsja {
	
	public static void main(String[] args) {
		DateFormat format = new SimpleDateFormat("dd.MM.YYYY");
		Date date = new Date();
		System.out.println("Leho, täna on " + format.format(date));
		System.out.println("Sisesta esimene number:");
		int yks = TextuuIO.getlnInt();
		System.out.println("Sisesta teine number:");		
		int kaks = TextuuIO.getlnInt();
		System.out.println("Korrutis:" + yks * kaks);		
	}
}
