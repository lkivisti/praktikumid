package niisama;

public class HashSet {

	public static void main(String[] args) {
	      System.out.println (keskmisestParemaid (new double[]{1,2,3,4}));
	}
	
	   public static int keskmisestParemaid (double[] d) {
		   int paremaid = 0;
		   double keskm = 0;
		   double summa = 0;
		   for (int i = 0; i < d.length; i++)
		   {
			   summa = summa + d[i];		   
		   }
		   keskm = summa / d.length;
		   System.out.println (keskm);

		   for (int i = 0; i < d.length; i++)
		   {
			   if (d[i]> keskm)
				   paremaid++;		   
		   }
	   
		   return paremaid;
       }
	   
//	   Koostage Java meetod, mis genereerib parameetrina etteantud n järgi niisuguse n korda n täisarvumaatriksi, mille iga elemendi väärtuseks on selle elemendi reaindeksi ja veeruindeksi summa (indeksid algavad nullist). 	   

	   
	   public static int[][] liitmisTabel (int n) {
		   int[][] maatrix = new int[n][n]; 
		   for (int i = 0; i < n; i++ )
			   for (int j = 0; j < n; j++ )
			   {
                   maatrix[i][j] = i+j;				   
			   }
		   return maatrix; 
	}
	   
//	   Sportlase esinemist hindab n>2 kohtunikku. Hinnete hulgast eemaldatakse kõige madalam ja kõige kõrgem ning leitakse ülejäänud n-2 hinde aritmeetiline keskmine.
//	   Kirjutada Java-meetod hinde arvutamiseks.
//	   Parameetriks olevat massiivi muuta ei tohi.
//	   There are n>2 judges and each gives a mark to sportsmans performance. Two extreme marks (one highest and one lowest) are removed and artithmetical mean of remaining n-2 marks is calculated.	   
	   
	   public static double result (double[] marks) {
		   double keskm = 0;
		   double summa = 0;
		   
		   double madalaim = marks[0];
		   double korgeim = marks[0];

		   for (int i = 1; i < marks.length; i++)
		   {
			   if (new Double(marks[i]).compareTo(korgeim) > 0)
					   korgeim = marks[i];
			   if (new Double(marks[i]).compareTo(madalaim) < 0)
				   madalaim = marks[i];			   
		   }
		   
		   
		   for (int i = 0; i < marks.length; i++)
		   {
			   if (!new Double(marks[i]).equals(madalaim) || !new Double(marks[i]).equals(korgeim))
				    summa = summa + marks[i];		   
		   }
		   keskm = summa / marks.length;
		    
		   return keskm;
	   }	   
}
